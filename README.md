# DebriefKit

Supports pour la partie debrief/débat de la Fresque du climat.

Ce kit est largement inspiré du jeux [inventons nos vies bas carbone](https://www.resistanceclimatique.org/inventons_nos_vies_bas_carbone) (INVBC) de Résistance Climatique.
Bien qu'il soit déjà utilisable, ce kit à vocation d'évoluer et de s'enrichir rapidement.

Au 18/10/2021, les cartes "empreintes" sont directement basée sur le rapport le plus à jour et officiel disponible ici : https://ree.developpement-durable.gouv.fr/themes/defis-environnementaux/changement-climatique/empreinte-carbone/article/l-empreinte-carbone-de-la-france. Les informations fournies au dos des cartes proviennent elles aussi directement de ce rapport, et non pas vocation à être transmises aux participants.

Voici quelques guidelines et différences avec INVBC pour le développement de ce kit :
- Être le plus à jour possible vis à vis des dernières estimations. Par exemple, INVBC donne une empreinte de 12 t CO2e / hab / an, alors que les dernières estimations sont plutôt de l'ordre de 10 t avec des différences très importantes sur certains postes.
- Un design simple permettant de s'adapter rapidement aux nouvelles estimations.
- Fournir un minimum d'information sur les cartes "empreintes" pour ne pas trop orienter les débats, et aussi éviter d'afficher des info ayant peu de sens car masquant de grande inégalité (par ex. l'avion).
- Les cartes détails doivent être conçues pour être les plus factuels possibles et éviter de trop dépendre d'une liste d'hypothèses difficiles à restituer.
    - Un exemple typique est l'alimentation. Plutôt que de présenter une empreinte annualisée pour un régime particulier et impossible à détailler, le parti pris est de montrer l'impact d'une certaine quantité de certains aliments à comparer au budget journalier (car on mange tous les jours !)
    - Aussi, plutôt que de résumer certains aspects par 2-3 cartes choisies arbitrairement, il peu être préférable de s'appuyer sur un graphique à l'échelle.

# Utilisation

Voici le déroulé typique d'une séance de débrief avec ce kit :

1. Après le tour des émotions, pour introduire ce kit, l'animateur peut, par exemple, commencer par rappeler que l'intensité et la fréquence des conséquences désastreuses sont liés à l'élévation de la T°, qui est elle même lié à l'augmentation du taux de GES, et donc à nos activité humaine. Ces désastres ne sont pas inéluctables si collectivement nous réduisons drastiquement ces émissions. Cf. les scénario des cartes.
2. Afin de savoir où nous en sommes au niveau de la France, l'animateur présente les 5 grands postes en les accrochant les uns après les autre. A ce stade pas de chiffre, on montre juste des proportions.
3. Une fois les 5 cartes posées, on fini avec la bulle donnant l'empreinte à 666 MtCO2e, qui, divisé par 66 millions de français, donne 10t. Rappeler que c'est une moyenne qui n'a peu de sens (un peu comme les 1.7 enfants par couple) et cache de grandes disparité.
4. Mentionner les accords de Paris qui se traduise par la carte "2 t" à placer en dessous pour l'effet entonnoir. Ces deux chiffres, 2 t/an et 5.5 t/j sont pratiques pour se rendre compte du coût d'une action fréquente ou ponctuel.
5. On lance les débats (post-it ou pas) et on sort les cartes "détails" au besoin pour étayer les discussions.

