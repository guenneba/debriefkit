# Numérique

On pourrai être tenter de mettre en avant la part du numérique en France.
Comme celui ci est dilué un peu partout, il faut passer par une carte dédiée.

Malheureusement, à l'heure actuelle, les études sur ce sujet ne sont vraiment pas très fiable (TheShiftProject) et/ou offre peu de transparence (GreenIT/iNum).

Par exemple, l'étude iNum donne une empreinte de 0.37t CO2e/hab dont 0.06t pour la consommation électrique (8% de la consommation). Mais cette étude est basée sur un inventaire combinée à des durées de vie "généralement constatées par les experts ayant participé à cette étude".
Cette approche n'est clairement pas très fiable, et si on se fit plutôt aux statistiques de ventes, alors on arrive plutôt à 0.2 t.

A cela, il faut ajouter 1) l'usage des datacenters et réseaux hors de France, et 2) les terminaux utilisés dans les entreprises pour fabriquer/gérer les produits/services que nous importons.

D'après l'étude d'Andrae et les mises à jour d'une méta-analyse récente [1], un ordre de grandeur pour datacenter+network (usage & fabrication) à l'échelle du monde serait de 370 MtCO2e/an.
Si on affecte ces émissions à la France au prorata du trafic entrant+sortant versus trafic mondiale (ce qui n'a pas forcément de sens physique, mais faute de mieux...), on obtient un ordre de grandeur de +0.2t, soit 4x plus qu'une division des 370Mt par 7.7 milliard d'hab., ce qui semble ok.

Ces 0.2t n'apparaissent pas dans les 10t de l'empreinte de la France, il sont donc en plus.
Aau passage, si l'électricité qui alimente ces DC et réseaux était aussi bas carbone qu'en France, ce chiffre chuterai à 0.06.

Pour ce qui est des terminaux utilisés dans les entreprises pour fabriquer/gérer les produits/services que nous importons, même au doigt mouillé c'est pas évident du tout !
Donc si on y va à la truelle les yeux bandés, toujours selon Andrae et [1], si on considère uniquement les terminaux, on fait l'hypothèse que 50% sont des équipements "pro", on divise par 7.7 milliard d'hab, et on multiplie par 4 car on importe beaucoup et qu'on est "riche", on arrive à +0.290t.
Ce chiffre représente 6% des émissions importées, ce qui semble déjà vraiment beaucoup, donc plutôt une fourchette haute.

Au total, si on rajoute les incertitudes énormes, ca ferait une fourchette entre 0.35 et 0.9. Nous voilà bien avancé !

[1] https://www.cell.com/patterns/fulltext/S2666-3899(21)00188-4