# Transports

Dans l'empreinte officiel, la phrase "2,3 t CO2 éq pour les véhicules particuliers (dont la consommation de carburants pour 1,1 t CO2 éq...)" est ambigu à plusieurs titres.
A priori, ici "véhicules particuliers" inclut vraiment tous les véhicule (avions, jets privées, bateaux, yacht, etc.) et "consommation de carburants" fait référence uniquement aux émissions directes dues à la combustion, c'est à dire hors amonts (énergie pour extraire, raffiner, transporter le carburant).
Mais pour les jets et les yachts, de la même façon que les avions à l'international ne sont pas comptabilisés, on peut se poser la question dans quelle mesure sont-ils comptabilisés...

En fait, ce qui nous intéresse vraiment ici dans le cadre du débrief, c'est plutôt la "consommation, amonts inclus, des voitures des particuliers".

Pour cela, nous pouvons nous appuyer sur les facteurs d'émissions amont et combustion du diesel et sans plomb (par ex., https://www.europarl.europa.eu/resources/library/images/20190321PHT32173/20190321PHT32173_original.jpg), et des quantité consommée par les voitures des particuliers (https://www.citepa.org/wp-content/uploads/2.5-Transports_2020.pdf).

Ce qui donne pour 67 millions d'habitants :
- amont = 190 kgCO2/hab/an
- combustion = 930 kgCO2/hab/an
- total = **1121** kgCO2/hab/an

## Questions